package com.pe.rl;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //Variable Global
    Button butOK;
    Button butCancelar;
    Spinner spiPaises;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Instanciamos los objetos
        butOK = findViewById(R.id.butOK);
        butCancelar = findViewById(R.id.butCancelar);
        spiPaises = findViewById(R.id.spiPaises);


        //Evento para reaccionar al click en el boton
        butOK.setOnClickListener(this);
        butCancelar.setOnClickListener(this);

        llenarSpinnerListado();

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.butOK) {
            butOK.setText("TExto cambiado desde el evento onClick");

            Toast.makeText(this, "Toast del evento click", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Click Cancelar", Toast.LENGTH_SHORT).show();
        }

    }

    private void llenarSpinnerListado() {
        List<String> lstPaises = new ArrayList<>();

        lstPaises.add("Peru");
        lstPaises.add("Argentina");
        lstPaises.add("Uruguay");
        lstPaises.add("Italia");
        lstPaises.add("Francia");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, lstPaises);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spiPaises.setAdapter(dataAdapter);

    }


    private void llenarSpinnerArray() {

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.array_paises));

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spiPaises.setAdapter(dataAdapter);
    }
}
